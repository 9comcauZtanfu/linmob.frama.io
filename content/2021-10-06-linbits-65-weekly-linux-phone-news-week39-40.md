+++
title = "LinBits 65: Weekly Linux Phone news / media roundup (week 39/40)"
date = "2021-10-06T21:20:00Z"
draft = false
[taxonomies]
tags = ["PinePhone", "Librem 5", "Purism", "NixOS", "Nemo Mobile", "Qt 6", ]
categories = ["weekly update"]
authors = ["peter"]
+++

_This week in Linux Phones:_  Nemo Mobile and Mobile NixOS progress reports, updates on Kalendar, KQuickImageEdior and some GNOME apps, a guide to PinePhone Privacy, yet more cross-compilation, a cool new podcast and many PinePhone videos! <!-- more --> _Commentary in italics._ 

### Worth noting
* The newly announced FairPhone 4 can apparently already boot [postmarketOS](https://wiki.postmarketos.org/wiki/Fairphone_4_(fairphone-fp4))! 
* Purism sent another email to Librem 5 backers (including me), telling buyers to [expect further delays](https://forums.puri.sm/t/librem-5-backlog-delivery-update-sep-oct-2021/14897). 

### Worth reading

#### Software progress
* Qt blog: [Qt 6.2 LTS Released](https://www.qt.io/blog/qt-6.2-lts-released). _Qt 6 is getting more and more developed, which matters as eventually KDE will do something cool with it!_
* This Week in GNOME: [#12 Inspecting Libadwaita](https://thisweek.gnome.org/posts/2021/10/twig-12/).
* Alexander Mikhaylenko: [Dark Style Preference](https://blogs.gnome.org/alexm/2021/10/04/dark-style-preference/).
* Volker Krause: [August/September in KDE Itinerary](https://thisweek.gnome.org/posts/2021/10/twig-12/). _Walking directions are a great new feature!_
* Carl Schwan: [KQuickImageEditor 0.2 released](https://carlschwan.eu/2021/10/02/kquickimageeditor-0.2-released/).
* Claudio Cambra: [Massive new features: a new week view, KCommandBar, and more — Kalendar devlog 17](https://claudiocambra.com/2021/10/03/massive-new-features-a-new-week-view-kcommandbar-and-more-kalendar-devlog-17/).
* Nemo Mobile: [Nemomobile in October/2021](http://nemomobile.net/pages/nemomobile-in-october-2021/). _Great progress!_
* Christian F. K. Schaller: [PipeWire and fixing the Linux Video Capture stack](https://blogs.gnome.org/uraeus/2021/10/01/pipewire-and-fixing-the-linux-video-capture-stack/). _Pipewire is not just important for phones._

#### Distro progress
* Mobile NixOS: [August and September 2021 round-up](https://mobile.nixos.org/news/2021-10-05-august-september-round-up.html).	_Plasma landing, nice!_

#### Hardware repair
* haskal: [repairing the pinephone](https://write.lain.faith/~/Haskal/repairing-the-pinephone/).

#### Tutorials
* Han Young: [Cross Compile to PinePhone Part Four](https://www.hanyoung.uk/blog/cross-compile-to-pinephone-part-four/).
* RTP: [PinePhone Privacy Ideas/Tips [Videos/Writeup]](https://politictech.wordpress.com/2021/09/30/top-pinephone-privacy-tips-videos-writeup/). _Great! While we've linked many of his videos in the past, having them curated in an explanatory blog post makes them even better!_

#### Off topic, but a fun read
* Amos B. Batto: [What can be learned by taking apart a Huawei smartphone](https://amosbbatto.wordpress.com/2021/10/06/huawei-smartphone/).

#### Competition
* fossphones.com: [Linux Phone News - October 4, 2021](https://fossphones.com/10-04.html). _Nice roundup!_

### Worth listening
* PEBKAC.show: [S01 E01: Mobian Project Interview](https://pebkac.show/web/2021/10/01/the-pebkac-show/). _Great first episode! PEBKAC BTW is short for "Problem Exists Between Keyboard And Chair"._

### Worth watching
#### Librem 5
* PizzaLovingNerd: [It's Finally Here! (Librem 5 Unboxing/Review)](https://tilvids.com/w/fcdFrjG48qmB8TLFjit6o1). _Nice video! Minor nitpick: IMHO, the biggest problem with Librem 5 right now is not price, but the uncertainty to when it is going to be delivered._ 

#### Current Manjaro
* TechHut: [Manjaro PHOSH on the PinePhone - Beta 16](https://www.youtube.com/watch?v=lDmY3xhZIX0). _Good review!_

#### postmarketOS ports
* Baonks 81: [postmarketOS Phosh on Xiaomi Redmi 2 wt86047_wt88047 kernel 5.14](https://www.youtube.com/watch?v=H_QlNfT6G60).

#### Tutorial
* (RTP) Privacy & Tech Tips: [Pinephone Privacy Tips](https://www.youtube.com/watch?v=bJ-jGKWQKJY). _Excellent!_

#### Conference talks
* Guido Günther: [Debian on a smart phone, are you serious?](https://saimei.ftp.acc.umu.se/pub/debian-meetings/2021/MiniDebConf-Regensburg/debian-on-a-smart-phone-are-you-serious.lq.webm). _Great talk, [this toot](https://social.librem.one/@agx/107050012218380100) has a link to the slides!_
* Fedora Project: [The PinePhone Hardware, Software and Next Steps](https://www.youtube.com/watch?v=PuEL_GJ1Y2s). 

#### Shorts
* NOT A FBI Honey pot: [is this the best pinephone distro? with Htop pre-installed! #pinephone #shorts #linux](https://www.youtube.com/watch?v=fkAZlR0Wcbk), _Maemo Leste, ladies and gentlemen!_
* Requested everywhere: [Custom Ringtone on PinePhone](https://www.youtube.com/watch?v=ZOY-IJSLT3Q).
* Drew Naylor 2: [](https://www.youtube.com/watch?v=SEua4VDVxM8). 
* Scientific perspective: [Dual boot Ubuntu touch + any android distro | POCO F1 #shorts](https://www.youtube.com/watch?v=IuanNIzLwdE).

### Stuff I did

#### Content
I played with Swmo (Sxmo, but Wayland), and wrote a [little blog post about it](https://linmob.net/playing-with-swmo/) - [right on the PinePhone running Swmo](https://fosstodon.org/@linmob/107051189274623959).

I have also started to write a post detailing my current Linux Phone setup, as my [post from January](https://linmob.net/my-setup-with-danctnix-archlinuxarm/) is slightly outdated. I hope to be done with it before the next weekly update.

#### LINMOBapps

I did next to nothing here (I wasted some time unsuccessfully trying to build one app), but at least [accepted a merge request](https://framagit.org/linmobapps/linmobapps.frama.io/-/merge_requests/16). Thank you so much, Ferenc!
[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master) on LINMOBapps this week. And please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)

#### Linux Phone Apps

Still only thoughts, the planned "let's do something on saturday" action was cancelled due to not feeling too well. 
