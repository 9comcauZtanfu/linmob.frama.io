+++
title = "a reMarkable paper tablet"
aliases = ["2018/04/23/a-remarkable-paper-tablet.html"]
author = "Peter"
date = "2018-04-23T16:40:13Z"
layout = "post"
[taxonomies]
tags = ["cloud", "DA", "digital assistant", "eInk", "eInk tablets", "reMarkable", "replacing paper", "tablets", "digital handwriting"]
categories = ["impressions"]
authors = ["peter"]
+++
_I am late on this one, as I could have written about this [way earlier](https://www.theverge.com/circuitbreaker/2016/12/2/13818796/remarkable-giant-e-ink-tablet-paper-notebook). But I neglected to do so because it seemed to be just another good idea that was not going to work out&mdash;neither in the market place, nor technologically._ <!-- more -->

After this dark precursor on a very white device, the **reMarkable paper  tablet** , let me start by pointing out that this is a device that comes close to fulfill my ["Digital Assistant" "vision" I uttered in late 2009](https://linmob.net/2009/11/06/on-tablets-or-the-da/). In fact, it is the device that comes the closest of the countless devices I had for brief intervals of time (* = still have it):

  * Apple iPad 2*, iPad Air, iPad Pro 12.9 + Apple Pencil
  * Samsung Chromebook Plus
  * ASUS VivoTab Note 8*
  * ASUS Chromebook Flip C100P*, C101P*
  * Tolino Tab 8*
  * Lenovo ThinkPad X230 Tablet
  * Barnes & Noble NOOK Simple Touch, NOOK Glowlight+*
  * Tolino Shine
  * Amazon Kindle DX*

If you have been following this blog, you may have noticed that I did only
write about a few devices on that list. This has two reasons:

  1. Since I gave up the idea of becoming a tech journalist or analyst, I have been amazingly lazy in writing my thoughts down. Also I have been quite critical of my own writing and whenever it felt like I had nothing to add, I did not finish nor publish my drafts.
  2. None of these devices got as close to that old DA idea as the reMarkable Tablet. Some just lacked the pen (all the eInk devices in the list, Tolino Tab and the Chromebooks Flip), others where just to heavy to feel good (Thinkpad X230T), and the remaining made me realize that reading and writing on an LCD screen is just not like reading or writing on paper (iPad Pro, Samsung Chromebook Plus).[^1]



**What's so great about this?**  
Now _why_ is the reMarkable better at replacing paper than all these
aforementioned devices? That's easy: It is so much closer to feeling like
paper than everything else I have tried before. They nailed the experience,
they did not only design a pen that feels great in the hand but one that keeps
feeling great after you used it for hours. Sure, you don't have colors, but
actually, that can make things easier. [^2] And: While I hated my handwriting
on the Apple and Samsung devices, I am just as ok with it on the reMarkable
paper tablet as I am on actual paper.[^3]

**Limitations**  
If you are curious about the specs, you may scroll down to the bottom now. As
this is not a device that wants to be your computer, what matters is, that the
experience is alright. For writing, I did not notice anything being slow yet.
With PDFs, the reMarkable tablet sometimes has to think a little before
displaying the next page after you hit the button, especially if there are
high resolution images on the page—but there are horrible PDFs out there and
lean ones do work just fine. There also have been complaints that the
annotation format reMarkable came up with is not perfect. As the reMarkable
can also be used to read .epub (it does not support DRM),[^4] some users
have been reporting problems with certain epub files. _I will expand this
section as I find out more._  

_Summary:_ The reMarkable is an ok reader, and if you have your personal PDF
and epub toolchain anyway because you had to fix similar problems for other
ereaders, you will be fine, but be aware that a tech illiterate relative may
need your help from time to time.

**The bad stuff**  
_Cloud._ reMarkable chose to make this a "cloud" dependend device. Just like
the devices interface comes with a Qt 5 based interface, to upload content to
your reMarkable you are supposed to use their software, which is build with Qt
5 and is available for Mac, Windows, Android and iOS. For desktop GNU/Linux,
there is some .deb file on the web, but it is not a supported platform, which
is sad but understandable given the plethora of linux distributions out there
and the market share of all these combined.  
Now why do I consider it bad to have this device be cloud-depending? In fact,
it is a feature that has some value: Not only are your notes being backed up
whenever your reMarkable paper tablet connects to a Wireless LAN it knows, you
can then access all your synced notes on your phone or computer. That is
definitively a feature, and I bet a majority of potential buyers appreciate
it.  
But as I am slightly paranoid and given that most of the processes on the
reMarkable paper tablet seem to run as root, I worry about their security
practices in general.[^5] Also, I don't want to
upload my thoughts to the NSA, GCHQ or some comparable agency. In order to
allow the reMarkable to become an extension of my brain, I therefore went
ahead and removed the Cloud service and disabled WiFi. I can still access my
notes over USB (they provide a web interface and you can SSH into your
reMarkable paper tablet easily). Getting PDFs on the remarkable works using a
[shell
script](https://github.com/adaerr/reMarkableScripts/blob/master/pdf2remarkable.sh).  

_Price._ Boy, is that thing expensive. EUR, USD 499 is the current price (a
limited time offer, afterwards it'll be USD 599), not including the folio
(which is a great addition, but adds another EUR, USD 79 to your bill). Now
while the new iPad (2018) + Apple Pencil will cost you less and be more
versatile, I would not want to swap my reMarkable paper tablet for that. Also,
if you compare it to other eInk devices with 10" or larger screens, the
reMarkables price is certainly comparable. Unfortunately, devices like the
larger Sony DP-RP1 pack better CPUs, which makes them quicker at those PDF
documents—still, a direct comparison is hard, as there (at least to my
knowledge) is no other device that matches the reMarkable paper tablets
features bit by bit.

**Conclusion.**  
If you want a device that is the closest to paper to replace paper, this
should be one of, if not the best, option. Should you buy one? Honestly I
don't know. But I know I don't regret having bought it one bit. I just love
it.

**Specifications:** [^6]  
_Size and weight:_

  * 177 x 256 x 6.7mm (6.9 x 10.1 x .26 inches)
  * 350 gram (.77 pounds)

_CANVAS Display_

  * 10.3” monochrome digital paper display (black and white)
  * 1872x1404 resolution (226 DPI)
  * Partially powered by E Ink Carta technology
  * Multi-point capacitive touch
  * No glass parts, virtually unbreakable
  * Paper-like surface friction
  * Palm rejection

_Storage and RAM_

  * 8 GB internal storage (6.5 GBs free, which should amount to 100.000 pages of notes according to reMarkable)
  * 512 MB DDR3L RAM

_Processor_

1 GHz ARM A9 CPU (NXP/Freescale i.MX6 SoloLite)

_Battery_

  * Rechargeable (Micro USB)
  * 3000 mAh (non user replacable)

_Operating system_

Codex, a custom Linux-based OS optimized for low-latency e-paper.

`Linux remarkable 4.1.28-fslc+g7f82abb #1 Tue Nov 21 19:03:42 CET 2017 armv7l
GNU/Linux`, busybox, systemd, Qt 5.9 for GUI, pdfium

_Connectivity_

  * WLan/WiFi 802.11 a/b/g/n (BCM 4330)
  * microUSB

**Further resources.**

  * [reMarkable Shop](https://remarkable.com/store/)
  * [The reMarkable Blog](https://blog.remarkable.com/)
  * [Subreddit](https://www.reddit.com/r/RemarkableTablet/)
  * [remarkablewiki.com](http://remarkablewiki.com/start) with many links to hacks and more information on the device


[^1]: In addition to that I really did not like the industrial design of the Apple Pencil, it felt to heavy; and the Pen thing of the Chromebook Plus on the otherhand felt to flimsy, just like the keyboard and the overall feel of the device made me think that I would likely break it soon, which was why I sold it quickly. With the ASUS VivoTab Note 8 I can't really say what the main issue is, as I have barely used it, and I used it with Ubuntu since the Windows install it came with was totally messed up. And while "[xournal](http://xournal.sourceforge.net/)" is nice and all, it does not stand a chance against its proprietary competition, e.g. Microsoft OneNote.
[^2]: No matter what color I chose on the Samsung Chromebook Plus, it felt slightly wrong to my eye and Apples Notes App on the iPad Pro did only slightly better.
[^3]: I know: I am vain, and this is my vanity speaking. But, ask yourself and think carefully before you dismiss this.
[^4]: So you either have to restrict yourself to DRM free eBooks, or liberate your books from DRM hell, which unfortunately is illegal in most of the western countries.
[^5]: On the other hand, this makes the reMarkable easily hackable, which to me is a huge point in its favor.
[^6]: [Source](https://support.remarkable.com/hc/en-us/articles/115004554289), augmented on my own.
