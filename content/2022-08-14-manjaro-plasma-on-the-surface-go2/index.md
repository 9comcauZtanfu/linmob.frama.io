+++
title = "Manjaro Plasma on the Microsoft Surface Go 2"
date = "2022-08-14T06:00:00Z"
updated = "2023-04-07T16:00:00Z"
draft = false
[taxonomies]
tags = ["Microsoft Surface Go 2", "Manjaro", "Plasma", "KDE", "tablet"]
categories = ["impressions"]
authors = ["Plata"]
[extra]
edit_post_with_images = true
update_note = "Enhance Thunderbird configuration"
+++

After years of using tablets with Android based operating systems such as LineageOS, I got tired of not receiving updates after some time. Why should I throw away working hardware just because there is no software support anymore? So I went looking and finally got me a Microsoft Surface Go 2 to install Manjaro Linux with KDE Plasma on.<br>A Microsoft device for Linux? It's a better match than you might expect.
<!-- more -->
So here's what I've found.

# Installation
As the Surface Go 2 runs on "regular" x86 hardware and is supported in the upstream Linux kernel, you can basically install any Linux distribution as you would on a laptop or desktop computer.

For the installation you will need a USB drive and a keyboard. The Surface Go 2 comes with only one USB-C port but USB-C docks should do (I've used the PinePhone docking bar). Without keyboard, you will not be able to complete the installation (see [this Calamares issue on GitHub](https://github.com/calamares/calamares/issues/2024)).

The [linux-surface wiki on GitHub](https://github.com/linux-surface/linux-surface/wiki/Surface-Go-2) suggests to set up a dual boot configuration with Windows for easier firmware updates. However, this is only feasible with the version with a 128GB SDD. It is not possible to shrink Windows enough with the version with a 64GB eMMC. As I own the 64GB version, this article does not cover the dual boot setup.

Before you can start with the Linux installation on the Surface, some UEFI settings must be changed. Hold volume up and power to open UEFI. Then, open `Security` and set `Secure Boot` to `Disabled` and in `Boot configuration` move `USB Storage` to the top.

There's nothing special about the installation itself &ndash; just download the ISO from https://manjaro.org/download/ and flash a USB drive (e.g. with [balenaEtcher](https://www.balena.io/etcher/)). Then, plug it into the Surface and hold volume down and power to boot from USB. If you own the version with 4GB RAM, make sure to enable swap.

# First Impressions
The out-of-box experience when booting Manjaro Plasma will, quite honestly, not knock your socks off.

The first thing you'll see is an SDDM login screen which is not scaled well and always shows a mouse pointer &ndash; even if no mouse is plugged in. There's no virtual keyboard, so you cannot even login without a physical keyboard attached.

After logging in to Plasma, the scaling is set to 100% only which makes everything rather small and not well suited for touch input. X11 as the standard window system simply isn't made for touch screens. Also here, no virtual keyboard is available. Moreover, automatic screen rotation doesn't work.

Fortunately, all of this can be fixed easily.

# Setup
## General
To enable automatic screen rotation, install `iio-sensor-proxy`.

## Plasma
The first thing you want to do is install `plasma-wayland-session` and select `Plasma (Wayland)` on the login screen (SDDM) to use it. Next, set the scale to 200% in System Settings > Display and Monitor > Display Configuration.

If a few apps are open, it can happen that the panel contains too many things and the app icons become very tiny (especially in portrait mode). To avoid this, move unnecessary panel icons (such as virtual keyboard, workspaces etc.) into the overflow menu of the system tray. This is done by setting the entry to `Hidden` in the `Entries` page of the system tray settings.

To achieve a consistent look of GTK apps, apply the KDE theme in System Settings > Appearance > Application Style > Configure GNOME/GTK Application Style.

Apart from those general settings, which are more or less necessary to use Plasma with touch input, I decided to tweak it a bit more towards an experience which feels more natural on a tablet. 

> As this is mainly personal preference, it might not be for you (especially if you plan to use a mouse sometimes).

I don't think that it's too useful to have floating windows or multiple, tiled windows on a small screen. So if I would maximize my windows all the time anyways, why not have this done automatically. Therefore, I added a rule in System Settings > Window Management > Window Rules which maximizes all normal windows (you don't want this for dialogs etc.).

<center>
  <img style="margin: 10px;" src="window-rules.png" alt="Plasma Window Rules" />
</center>

In my opinion, the most important feature is touch gestures. This can be enabled in System Settings > Workspace Behavior > Touch Screen: I configured it such that swiping down from the top opens the application launcher and swiping up from the bottom opens the overview. This works with the regular application launcher, but also with custom widgets. I use [Stupid Simple Launcher](https://store.kde.org/p/1584342), and it's just great on a touch screen.<br>
_NOTE:_ The application launcher gesture requires that you're using an application launcher widget and it has a keyboard shortcut set (see [KDE bug 419188](https://bugs.kde.org/show_bug.cgi?id=419188)).<br>
For more information on gestures, check out the [KDE Community Wiki](https://community.kde.org/KDE_Visual_Design_Group/Gestures).

As I could then switch to a different application or close an application using the overview gesture, the window list in the panel was not required anymore and I removed it. Also, I chose to hide the window title bar by adding a window-specific override for all windows (regular expression `.*`) in System Settings > Appearance > Window Decorations. This frees up some precious vertical space.<br>
To remove the minimize, maximize and close button also for GTK apps, drag and drop them out of the titlebar in System Settings > Appearance > Window Decorations > Titlebar Buttons.

Finally, I moved the panel to the top and made it small which looks a bit like an Android-ish status bar.

The video below showcases this setup. You will need some imagination for the swipe gestures as it's only a recording of the screen.

<video width="480" height="320" controls>
  <source src="stupid-simple-launcher.mp4" type="video/mp4">
</video>

### Plasma Mobile Phone Homescreen
Alternatively to the application launcher widget, it is also possible to use the Plasma Mobile Phone Homescreen. After installing `plasma-mobile` from the [AUR](https://wiki.manjaro.org/index.php/Arch_User_Repository), it can be activated in the desktop settings > Configure Desktop and Wallpaper > Layout "Phone Homescreen".[^1]

As the application launcher widget is not needed anymore with this setup, you might want to remove it from the panel and map the swipe down gesture in System Settings > Workspace Behavior > Touch Screen to "Show Desktop" instead to easily reach the Phone Homescreen.

Here's some footage:

<video width="480" height="320" controls>
  <source src="phone-homescreen.mp4" type="video/mp4">
</video>

I find the Phone Homescreen to be a bit cluttered &ndash; especially with the default applications from Waydroid (see [this GitHub issue](https://github.com/waydroid/waydroid/issues/46)). To improve this, unused applications can be hidden by adding `NoDisplay=true` to the `.desktop` file.

## Virtual Keyboard (Maliit)
The reference virtual keyboard for Plasma, which is also used for Plasma Mobile, is [Maliit](https://maliit.github.io/). It can be installed as `maliit-keyboard`. Activate it in System Settings > Input Devices > Virtual Keyboard.

There's no GUI to configure Maliit for Plasma desktop yet (see [this GitHub issue](https://github.com/maliit/keyboard/issues/128)). Therefore, `gsettings` must be used instead.<br>
To add/remove languages run e.g. for German keyboard layout:
```
gsettings set org.maliit.keyboard.maliit enabled-languages "['en', 'de', 'emoji']"
```
Auto-completion can be very annoying at times. Especially, if it replaces your commands in Konsole with some entry from the dictionary. I turned it off with:
```
gsettings set org.maliit.keyboard.maliit auto-completion false
```
For an overview of all available settings, see the [schema on GitHub](https://github.com/maliit/keyboard/blob/master/data/schemas/org.maliit.keyboard.maliit.gschema.xml) or `gsettings list-keys org.maliit.keyboard.maliit`.

## SDDM
The login screen settings can be ìmproved to achieve a more touch friendly user interface. Edit `/etc/sddm.conf` to do so.

Enable the virtual keyboard by removing `InputMethod=`. There is a `/etc/sddm.conf.d/virtualkbd.conf` which should enable the virtual keyboard by default as described in [ArchWiki/SDDM](https://wiki.archlinux.org/title/SDDM) but the empty setting in `/etc/sddm.conf` overwrites it. It seems that the intention is to have the virtual keyboard by default and a bug in the packaging avoids it.

To scale the interface, add ` -dpi 144` to `ServerArguments`. The correct value for the 10.5" screen with a resolution of 1920x1280px would be 220dpi but then the complete layout is messed up and you get overlapping texts.<br>
_NOTE_: There's a setting `EnableHiDPI=true` but that didn't work for me.

Finally, hide the mouse cursor with ` -nocursor` appended to `ServerArguments`. I would expect that this happens automatically (see [this GitHub issue](https://github.com/sddm/sddm/issues/1571)).

# Applications
I will not deep dive into an evaluation of applications here as this would exceed the scope of this article. In general, [LinuxPhoneApps](https://linuxphoneapps.org/apps/) contains a list of touch friendly apps which work on small screens. Those should be good choices for tablets as well. Additionally, several desktop apps which can't really be used on phones might still be okay to use on a tablet.

## Firefox
If you run Firefox with X11/XWayland, touch input (especially scrolling) just isn't fun. However, it works very decently with Wayland. To enable this, add `MOZ_ENABLE_WAYLAND=1` to `/etc/environment`.

## E-Mail
### Geary
The most touch friendly e-mail client I could find would be [Geary](https://wiki.gnome.org/Apps/Geary). Unfortunately, there's currently a [bug](https://bugs.kde.org/show_bug.cgi?id=465919) which makes it almost useless: The virtual keyboard disappears when trying to write an e-mail. I really hope that this will get fixed soon because it works very nicely out of the box otherwise.

### KMail
If you're looking for a KDE app, consider [KMail](https://apps.kde.org/kmail2/). It gets the job done, but you will definitely feel that it's not made for touch and screens of this size. For example, the reading pane is rather small. Also, it is not possible to scroll long menu lists, which makes some entries unreachable (at least in landscape mode).

### Thunderbird
Of course, [Thunderbird](https://www.thunderbird.net/) must be mentioned here as well. With View > Density > Touch for a touch friendly spacing, the Menu and Status Bar disabled and some columns removed (e.g. keep only Read, From, Subject, Date), it is fine in landscape mode. Portrait mode is ok if you hide the folder pane and/or remove more columns from the message list.

By default, the message pane shows a huge header section with sender, receiver and subject, which leaves only very little space for the actual e-mail content. To solve this, either disable the message pane completely via View > Layout > Message Pane (off) and double-click to open e-mails in a new tab, or install the [Thunderbird Conversations](https://addons.thunderbird.net/en-US/thunderbird/addon/gmail-conversation-view/) add-on which replaces the header with a small bar.

Additionally, I'm using the [Hide Local Folders for TB78++](https://addons.thunderbird.net/en-US/thunderbird/addon/hide-local-folders-for-tb78/) add-on to clean up the folder pane a bit.

The upcoming [GUI redesign](https://twitter.com/mozthunderbird/status/1542121814180220928) will probably further improve the user experience.

## Terminal
The default [Konsole](https://konsole.kde.org/) works, but using it is rather uncomfortable. In a terminal, you need at least a "tab" key for autocomplete and an "up" key to recall the last commands. However, those are not available. It is possible to swipe up and down to navigate the history after a long press on space in Maliit, but that's not really intuitive.

Fortunately, [QMLKonsole](https://apps.kde.org/qmlkonsole/), which is developed especially for mobile devices, solves these issues.<br>
If the key toolbar is missing (this is the case e.g. when using the Flatpak, see [KDE bug 466252](https://bugs.kde.org/show_bug.cgi?id=466252)), add `KDE_KIRIGAMI_TABLET_MODE=1` to `/etc/environment`.

## 2FA
The KDE two-factor authentication app [Keysmith](https://apps.kde.org/keysmith/) works. If you're just looking for something to provide your TOTP/HOTP, it has you covered.

However, it does not provide any "advanced" functionality like QR code scanning or import/export. For these features, check out [Authenticator](https://flathub.org/apps/details/com.belmoussaoui.Authenticator).

## Waydroid
[Waydroid](https://waydro.id/) can be used to run Android apps on Linux with Wayland.

Install `waydroid` from the [AUR](https://wiki.manjaro.org/index.php/Arch_User_Repository) and run `sudo waydroid init`. After `sudo systemctl start waydroid-container`, start Waydroid with `waydroid show-full-ui`. It is also possible to install apps directly, e.g. F-Droid via `waydroid app install F-Droid.apk`.

If closing the Waydroid window does not work, stop it with `waydroid session stop`.

The biggest issue with Waydroid is that screen rotation does not work properly yet: Waydroid keeps the original width/height of the window before rotating it (see [this GitHub issue](https://github.com/waydroid/waydroid/issues/248)).

<center>
  <img style="margin: 10px;" src="waydroid.png" alt="Waydroid Rotation" />
</center>

## Feed Reader
Currently, the best KDE feed reader for a tablet is [Syndic](https://github.com/cscarney/syndic). Apart from a bug which can lead to an unintended selection of multiple articles (see [this GitHub issue](https://github.com/cscarney/syndic/issues/75)), which will hopefully get fixed soon, it works very nicely.

Maybe [Alligator](https://apps.kde.org/alligator/) might become a viable alternative in the future. Right now, it cannot show the complete content of an article (see [this open merge request](https://invent.kde.org/plasma-mobile/alligator/-/merge_requests/32)). This means that for many feeds only a teaser text is displayed, which makes it rather useless (at least for me).

A GNOME alternative is [Feeds](https://gfeeds.gabmus.org/).<br>
_NOTE:_ Use the Flatpak (the version in the `community` repository is outdated). Install `xdg-desktop-portal-gnome` to avoid font aliasing and missing icons.

## Plasma Mobile Gear
KDE provides a bunch of applications developed for [Plasma Mobile](https://plasma-mobile.org/) (and thus optimized for touch and small screens) which are packaged as [Plasma Mobile Gear](https://plasma-mobile.org/info/).

# Issues
## Plasma
The main issue in Plasma is that not all controls fit on the screen properly with 200% scaling. Most often, this happens in portrait mode. Examples are [Pamac](https://wiki.manjaro.org/index.php/Pamac) but also the Plasma Settings (e.g. SDDM page). Similarly, there is text which overlaps buttons when editing the panel (see [KDE bug 450892](https://bugs.kde.org/show_bug.cgi?id=450892)).

<center>
  <img style="margin: 10px;" src="settings-sddm.png" alt="SDDM Settings Page" />
</center>

Right after login, a mouse cursor is shown. It disappears when touching the screen for the first time (see [KDE bug 330006](https://bugs.kde.org/show_bug.cgi?id=330006)).

The search field in the overview effect, which I open with the swipe up gesture, does not open the virtual keyboard (see [KDE bug 457583](https://bugs.kde.org/show_bug.cgi?id=457583)).

## SDDM
With the setup described above, the SDDM login screen is very usable on a touch screen. The only small improvements I would wish for are related to the virtual keyboard: It would be nice if it would open automatically when selecting the password field (see [this GitHub issue](https://github.com/sddm/sddm/issues/1572)). Moreover, I would like to use Maliit (instead of qtvirtualkeyboard) to have the same virtual keyboard as in Plasma.

## Lock Screen
When an external screen is connected, it can happen that the virtual keyboard becomes very tiny (see [KDE bug 456826](https://bugs.kde.org/show_bug.cgi?id=456826)) or opens on the wrong screen (see [KDE bug 456825](https://bugs.kde.org/show_bug.cgi?id=456825)).

## Cameras
The cameras do not work out of the box. According to [GitHub/surface-linux](https://github.com/linux-surface/linux-surface/wiki/Camera-Support), the cameras should work with their kernel patches but I didn't bother digging into it because I don't need them.

# Accessories
The [PinePhone docking bar](https://pine64.com/product/pinephone-usb-c-docking-bar/) works. I've used it to connect a USB stick, mouse, keyboard and an external screen.

I do not own the [Type Cover](https://www.microsoft.com/en-us/d/surface-go-type-cover-english/8vszf6wtwf29) or a [pen](https://www.microsoft.com/en-us/d/surface-pen/8zl5c82qmg6b) so I couldn't test those.[^2]

# Summary
Can you turn a Microsoft Surface Go 2 into a Linux tablet with Manjaro Plasma?

I'll say: Yes, you can. If you can use it as a daily driver, will, of course, depend heavily on the applications you need.

Do not expect a touch experience which is on par with Android or iOS, though (well, at least yet). With the right settings, it feels like a tablet (if that sentence makes any sense) but there are still several places where you will notice that you're running an operating system/desktop environment which was made for larger screens. However, none of those had a big impact on usability in my testing &ndash; it mainly meant I had to turn to landscape to reach some buttons. Also, I'm quite confident that those remaining issues will get sorted out rather sooner than later. Generally, tablets will certainly profit from the developments around Linux on phones (like the PinePhone) as well.

With desktop environments like KDE Plasma running nicely on x86 tablets, it would be great to see distributions supporting this more. This means mainly a virtual keyboard included by default and touch friendly SDDM settings but it starts with the installation already. From what I've seen so far, only [KaOS](https://kaosx.us/) have added a virtual keyboard to their [Calamares](https://calamares.io) based installer.

I'm looking forward to what the future will hold.

# Contribute
The main purpose of this article is to provide some insight on the current state of (Manjaro) Linux with KDE Plasma on the Microsoft Surface Go 2 when used as a tablet. Moreover, it shall describe the required or useful settings for those who'd like to try it out themselves.

Therefore, I hope that it's not written once and then set in stone forever but rather evolves over time.

So, please, if you have any suggestions or questions, head over to [Framagit/LINMOB.net](https://framagit.org/linmob/linmob.frama.io) and open a merge request or issue.

# Links
- [Microsoft Surface Go 2](https://www.microsoft.com/en-us/d/surface-go-2/8pt3s2vjmdr6)
- [GitHub linux-surface](https://github.com/linux-surface/linux-surface/wiki/Surface-Go-2)
- [Manjaro](https://manjaro.org/)
- [KDE Plasma](https://kde.org/plasma-desktop/)
- [KDE Plasma 5.25](https://kde.org/announcements/plasma/5/5.25.0/)
- [Plasma Mobile](https://plasma-mobile.org/)
- [Plasma Mobile Gear](https://plasma-mobile.org/info/)
- [Maliit](https://maliit.github.io/)
- [LinuxPhoneApps](https://linuxphoneapps.org/apps/)
- [Waydroid](https://waydro.id/)
- [ArchWiki/Tablet PC](https://wiki.archlinux.org/title/Tablet_PC)



[^1]: Thanks [@ivanrancic](https://mastodon.social/@ivanrancic) for the [hint](https://mastodon.social/@ivanrancic/108823338579744664).
[^2]: __Editors note:__ Owning both accessories and having used them both successfully on two other distributions (KDE Neon and Fedora Workstation), I can't think of any reason why Type Cover or Pen would not work with Manjaro, too. ~linmob
