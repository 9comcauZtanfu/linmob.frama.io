+++
title = "Weekly GNU-like Mobile Linux Update (42/2022): A Linux Tablet and a Dying Meme"
date = "2022-10-23T22:27:00Z"
draft = false
updated = "2022-10-24T21:18:09Z"
[taxonomies]
tags = ["PINE64 Community Update","postmarketOS","Plasma Mobile",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
update_note = "KNOME has not happened yet, my tired eyes just missed that my inprecise fingers had removed one line they should not have removed."
author_extra = " (with friendly assistance from plata's awesome script)"
+++

And obviously a lot more, including that GNOME Web 44 is going to be GTK4-based and Nemo Mobile is moving things to Qt 6.

<!-- more -->

_Commentary in italics._

### Hardware
* [Juno Computers Announces New Tablet for Preorder » Linux Magazine](https://www.linux-magazine.com/Online/News/Juno-Computers-Announces-New-Tablet-for-Preorder). _That Jasper Lake chip may be a good compromise between performance and battery life, and - as we all know - Intel just is easier on end-users than anything ARM._
   - Lemmy - linuxphones: [Juno tablet runs Manjaro Linux or Mobian on a Celeron N5100 Jasper Lake CPU](https://lemmy.ml/post/546327)
   - Purism community: [Linux Tablet PC with Mobian](https://forums.puri.sm/t/linux-tablet-pc-with-mobian/18458)

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#66 Foundation Updates](https://thisweek.gnome.org/posts/2022/10/twig-66/). _GTK4 Epiphany has landed, awesome!_
- Georges Stavracas: [the death of the meme](https://feaneron.com/2022/10/21/the-death-of-the-meme/). _Thumbnails in a file picker? What?_


#### Plasma ecosystem	
- Nate Graham: [This week in KDE: UI improvements abound](https://pointieststick.com/2022/10/21/this-week-in-kde-ui-improvements-abound-2/)
- Nate Graham: [On hiring, and fundraising to make it more biggerer](https://pointieststick.com/2022/10/17/on-hiring-and-fundraising-to-make-it-more-biggerer/)
- dot.kde.org: [KDE's Google Summer of Code 2022 Projects: Final Report](https://dot.kde.org/2022/10/22/kdes-google-summer-code-2022-projects-final-report)
- Volker Krause: [OSM Indoor Mapping Workshop Recap](https://www.volkerkrause.eu/2022/10/22/osm-indoor-workshop-october-2022.html)
- dot.kde.org: [Akademy 2022 - The Weekend of KDE Talks, Panels and Presentations](https://dot.kde.org/2022/10/16/akademy-2022-weekend-kde-talks-panels-and-presentations)

#### Nemo Mobile
- neochapay on twitter: [lets make some magic https://github.com/sailfishos/mlite/pull/4 move mlite to Qt6 #nemomobile #glacierUX](https://twitter.com/neochapay/status/1583060213170851840#m)

#### Sailfish OS
* [Sailfish Community News 10/20](https://forum.sailfishos.org/t/sailfish-community-news-20th-october-community/13306)


#### Distributions
* postmarketOS Blog: [v22.06 SP3: The One Where We Upgraded All The Kernels](https://postmarketos.org/blog/2022/10/23/v22.06.3-release/). _You know, that nasty Wireless LAN CVE..._
- Mobian Wiki: [install-x86 - fix broken links, add comment about status](https://wiki.mobian-project.org/doku.php?id=install-x86&rev=1666268612&do=diff)
- Manjaro PinePhone Plasma Mobile: [Release 202210211033](https://github.com/manjaro-pinephone/plasma-mobile/releases/tag/202210211033). _Looks like a new 'beta release' might be coming soon-ish. I managed to test this very briefly on my PinePhone Pro, and ... Plasma Mobile 5.26 is definitely nice!_


#### Non-Linux

#### Kernel
- Phoronix: [Mesa Lands DMA-BUF Feedback Support For Vulkan On Wayland](https://www.phoronix.com/news/DMA-BUF-FB-Mesa-VLK-Wayland)
- Phoronix: [Mesa Git Makes It Easier Activating Rusticl OpenCL Device Support](https://www.phoronix.com/news/Mesa-RUSTICL_ENABLE)


#### Matrix
- Matrix.org: [This Week in Matrix 2022-10-21](https://matrix.org/blog/2022/10/21/this-week-in-matrix-2022-10-21)

### Worth noting
* [libcamera/libcamera.git - libcamera 0.0.1](https://git.libcamera.org/libcamera/libcamera.git/tag/?h=v0.0.1). _I missed this, when it happened._
* [Enlightenment Window Manager r/PinePhoneOfficial](https://www.reddit.com/r/PinePhoneOfficial/comments/y6ebpf/enlightenment_window_manager/). _In case you're into Enlightenment, here's a thread to learn from or share your learnings!_

### Worth reading
* Chris Vogel: [storing passwords and using a 2nd factor for authentication](https://chrichri.ween.de/o/8664afbd7be046e590b825c371b0c017)
* Lup Yuen Lee: [NuttX RTOS for PinePhone: Display Driver in Zig](https://lupyuen.github.io/articles/dsi2)
- Purism: [Librem 5 Vs. Librem 5 USA](https://puri.sm/posts/librem-5-vs-librem-5-usa/)
- Tobias Bernard: [Post Collapse Computing Part 3: Building Resilience](https://blogs.gnome.org/tbernard/2022/10/22/post-collapse-computing-3/)


### Worth listening
* postmarketOS podcast: [#24 Akademy 2022 Special](https://cast.postmarketos.org/episode/24-Akademy-2022-special/)
* Destination Linux: [298: Pine64 Interview with Lukasz Erecinski - Destination Linux - TuxDigital](https://tuxdigital.com/podcasts/destination-linux/dl-298/)


### Worth watching
- Lup Yuen Lee: [#PinePhone flying thru fractals ... On Apache #NuttX RTOS](https://www.youtube.com/watch?v=toC9iiPRwRI)
- Purism: [Librem 5 Vs. Librem 5 USA](https://www.youtube.com/watch?v=bq4hHM2vJiA)
- TechyNoob: [Ubuntu Touch on OnePlus 6T](https://www.youtube.com/watch?v=yi2PaE9Dj8Q)
- Atam Shkai: [Ubuntu Touch On Android](https://www.youtube.com/watch?v=rd4vzKN_jd8). _That's not Ubuntu Touch._
- FIXME Hackerspace: [Your Phone & You - Sailfish OS and Privacy](https://www.youtube.com/watch?v=OSNtmT_ITvc)
- FIXME Hackerspace: [Your Phone & You - Signal on Sailfish OS](https://www.youtube.com/watch?v=01R-rOuk4yU)
- Continuum Gaming: [Microsoft Continuum Gaming E335: SailfishOS as a 2nd OS on Volla Phone 22](https://www.youtube.com/watch?v=-uidxJKgO3c)
- LInux User Space: [The History of Alpine Linux](https://www.youtube.com/watch?v=uypElk4_8TM)
* Cahlen: [Linux Pinephone](https://odysee.com/@Cahlen:0/CahlenLee_20221020_LinuxPinephone:1)

### Thanks

Huge thanks to at least one anonymous contributor and also to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email - or just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)
PPS: This one has less headlines - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update%2039&body=Less%20or%20more%20headlines%20going%20forward%3F)
