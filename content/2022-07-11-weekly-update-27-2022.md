+++
title = "Weekly GNU-like Mobile Linux Update (27/2022): Summertime or postmarketOS on Pixel 3"
date = "2022-07-11T21:33:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","Minecraft","GNOME","Google Pixel 3","Mesa",]
categories = ["weekly update"]
authors = ["peter"]
+++
Not too much to highlight this week, but there are some nice podcasts to listen too and some videos to watch!
<!-- more -->
_Commentary in italics._

### Hardware enablement
* [Caleb made some progress with regard to Google Pixel 3](https://twitter.com/calebccff/status/1545906788431790083). _Be kind and don't pester your hardware-enabling developers when things are just booting!_ 

### Software progress

#### GNOME ecosystem
* This Week in GNOME: [#51 About New Apps](https://thisweek.gnome.org/posts/2022/07/twig-51/).
* Marco Melorio: [GSoC update #2 – Bouncing cards](https://melix99.wordpress.com/2022/07/09/gsoc-update-2-bouncing-cards/). _Nice progress for Fractal!_
* Squeekboard has seen a [minor release, updating a few translations and layouts](https://gitlab.gnome.org/World/Phosh/squeekboard/-/merge_requests/560/diffs?commit_id=0c99067b4bcfd12055965507b9fc797305c955f2).
* Initial Alphas of [GNOME Shell](https://github.com/GNOME/gnome-shell/commit/022359f55ba1c6ff5f10e1931e9148f87d863394) and [Mutter](https://github.com/GNOME/mutter/commit/60e13fde09d1319f6002c83edfcbfecf24adb275) have been released.

#### Plasma/Maui ecosystem

* Nate Graham: [This week in KDE: big things](https://pointieststick.com/2022/07/09/this-week-in-kde-big-things/). _Nice progress!_
* Felipe Kinoshita: [My week in KDE: Licenses](https://fhek.gitlab.io/en/my-week-in-kde-licenses/).
* Carl Schwan: [KDE PIM in May and June](https://carlschwan.eu/2022/07/04/kde-pim-in-may-and-june/).
* KDE Announcements: [KDE Ships Frameworks 5.96.0](https://kde.org/announcements/frameworks/5/5.96.0/).

#### Ubuntu Touch
* UBports: [UBports Training Update 5 aka 0101: Found a way to push data from the phone to the WebViewer!](https://ubports.com/blog/ubports-news-1/post/ubports-training-update-5-aka-0101-found-a-way-to-push-data-from-the-phone-to-the-webviewer-3857).

#### Distributions
* [PinePhone Pro support has been merged to the postmarketOS Installer](https://twitter.com/braam_martijn/status/1546163096456159232?cxt=HHwWgICzkZ7JifUqAAAA)!

#### GUI Toolkits
* Phoronix: [wxWidgets 3.2 Released After 15k+ Commits, HiDPI, Initial Wayland Support](https://www.phoronix.com/scan.php?page=news_item&px=wxWidgets-3.2-Released). _HiDPI, Wayland - sounds like wxWidgets is now more suitable for phone apps!_

#### Graphics
* Phoronix: [MSM DRM Driver Adds Adreno 619 Support With Linux 5.20](https://www.phoronix.com/scan.php?page=news_item&px=MSM-DRM-Driver-Linux-5.20).
* Phoronix: [Mesa 22.2 Pushed Back By Two Weeks To Let More Features Land](https://www.phoronix.com/scan.php?page=news_item&px=Mesa-22.2-Two-Week-Delay).

### Worth noting
* The Popcorn Computer Pocket PC is [now in production](https://blog.origin.popcorncomputer.com/2022/07/08/production-has-commenced/).
* The [PINE64 EU Store](https://pine64eu.com/) launched (after some delays due to excessive red tape). _Pricing may seem high, but I'd argue it's not: The speed of shipping and a two years warranty (instead of 30 days) are valuable, and there are some operational costs, consider storage and logistics operation alone. Plus, there's stuff like compliance with common market standards (e.g. a charge for recycling batteries). If I were to get another PINE64 device, I'll gladly pay extra for convenience. If you think differently: Ordering on PINE64.com is still possible for EU customers._

### Worth reading
#### Ubuntu Touch
* TuxPhones: [Google Pixel 3A achieves 100% Ubuntu Touch support score](https://tuxphones.com/google-pixel-3a-full-stable-ubports-linux-ubuntu-touch-feature-support/).

#### SIP Calling on GNOME and Phosh
* Evangelos Ribeiro Tzaras for Purism: [Voice over IP in GNOME Calls Part 1: SIP Protocol and Libraries](https://puri.sm/posts/voice-over-ip-in-gnome-calls-part-1-sip-protocol-and-libraries/).

#### Apps
* LinuxPhoneApps.org: [New apps of LinuxPhoneApps.org, Q2/2022 🎉](https://linuxphoneapps.org/blog/new-listed-apps-q2-2022/). _No news for those who follow LPA closely like I do ;-)._

### Worth listening
* PINE64: PineTalk [Season 2, Episode 9: Gaming on Pine64 is a Thing](https://www.pine64.org/2022/07/10/s02e09-gaming-on-pine64-is-a-thing/). _Nice episode!_
* Linux After Dark: [Episode 21](https://latenightlinux.com/linux-after-dark-episode-21/). _Glad I went with Neon and not Kubuntu for my Surface Go 2 experiments!_
* postmarketOS Podcast: [#19 apk fix, qbootctl, Phosh 0.20, iPhones and more Apples](https://cast.postmarketos.org/episode/19-apk-fix-qbootctl-phosh-0.20-iPhones-and-more-Apples/).[^1] _Another great episode!_

### Worth watching

#### Manjaro Phosh
* Avisando: [PinePhone - Manjaro Phosh (0.20): What's new?](https://www.youtube.com/watch?v=-BSR1b0SVLU). _Please note: The new App Drawer is not going to ship with 0.20.0, same for the multitasking view - Manjaro included a few unmerge feature Merge Requests (i.e. drafts) on top of the 0.20.0 beta[.](https://do-not-ship.it/)_

#### GNOME App Improvements
* Geotechland: [GNOME 42 Mobile Apps On Pinephone](https://www.youtube.com/watch?v=--874pAWgKQ). _Nice progress!_
* baby WOGUE: [An adaptive sidebar for Files, Ft GNOME 43 & ..Ubuntu](https://www.youtube.com/watch?v=y6jPVKcsxjo).
* baby WOGUE: [A view on Loupe, the upcoming Image Viewer of GNOME](https://www.youtube.com/watch?v=9T-YU6xb3tU)

#### Hardware fixing
* Fast Code Studio: [PinePhone Keyboard Fixed!](https://www.youtube.com/watch?v=aXIw0i-c-Ms). _Make sure to also read the [reddit post](https://www.reddit.com/r/PINE64official/comments/vw4s8t/using_a_usb_device_with_the_pinephone_keyboard/)._

#### Reviving hardware with postmarketOS and Ubuntu Touch
* Mumbling Hugo: [Linux on Android: can old dog pick up new tricks?](https://www.youtube.com/watch?v=dB7vurJxT_c).

#### Gaming 
* Supositware: [Minecraft java 1.16.5 on the OG PinePhone](https://www.youtube.com/watch?v=qa_52C7NWWM). _Pushing AllWinner A64 to its limits!_

### Something missing?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)

[^1]: Since I originally had forgotten to include this on publication last week, it's in here again for the early readers!
