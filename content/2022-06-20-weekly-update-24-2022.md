+++
title = "Weekly #LinuxPhone Update (24/2022): postmarketOS 22.06 and other software progress"
date = "2022-06-20T22:05:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","PinePhone modem firmware","Sailfish OS","Plasma Desktop","Ubuntu Touch","Volla Community Days","Gaming handhelds","RK3566",]
categories = ["weekly update"]
authors = ["peter"]
+++

_Call me a postmarketOS shill or don't, Modem and Modem Firmware news, and a lot more!_

<!-- more -->
_Commentary in italics._

### Software progress

#### PinePhone Modem Firmware
* Biktorgj: [Release 0.6.7 Fixing some bugs and making the firmware easier to identify](https://github.com/Biktorgj/pinephone_modem_sdk/releases/tag/0.6.7). _Nice. [It can even run DOOM!](https://twitter.com/biktorgj/status/1538407447873916928)_


#### GNOME ecosystem
* This Week in GNOME: [#48 Adaptive Calendar](https://thisweek.gnome.org/posts/2022/06/twig-48/). _Please note: GNOME Calendar is not fully adaptive yet._
* Juan Pablo: [Cambalache 0.10.0 is out!](https://blogs.gnome.org/xjuan/2022/06/15/cambalache-0-10-0-is-out/)
* Antonio F.: [The tree view is undead, long live the column view‽](https://blogs.gnome.org/antoniof/2022/06/15/the-tree-view-is-undead-long-live-the-column-view%e2%80%bd/). _Looking forward to a new Nautilus, hopefully more adaptive, too!_
* Marco Melorio: [GSoC update #1 – Planning – Space Penguin](https://melix99.wordpress.com/2022/06/17/gsoc-update-1-planning/). 

#### Plasma/Maui ecosystem

* Nate Graham: [This week in KDE: non-blurry XWayland apps!](https://pointieststick.com/2022/06/17/this-week-in-kde-non-blurry-xwayland-apps/).
* KDE Announcements: [Plasma 5.25](https://kde.org/announcements/plasma/5/5.25.0/). _Yes, just a desktop release – but an important one for tablets!_
* Carl Schwan: [Kalendar Contact Book - Current Progress](https://carlschwan.eu/2022/06/14/kalendar-contact-book-current-progress/).
* snehit.dev: [Adding Spaces Horizontal Bar to NeoChat - GSoC'22 post #3](https://snehit.dev/posts/kde/gsoc-22/adding-spaces-list/).
* TSDgeos' blog: [The KDE Qt5 Patch Collection has been rebased on top of Qt 5.15.5](https://tsdgeos.blogspot.com/2022/06/the-kde-qt5-patch-collection-has-been.html). _Progress!_
* Felipe Kinoshita: [My week in KDE: Plasma, REUSE and Apps](https://fhek.gitlab.io/en/my-week-in-kde-plasma-reuse-and-apps/). _So many apps in one week!_
* Volker Krause: [Android Platform Calendar Access](https://www.volkerkrause.eu/2022/06/18/kf5-android-platform-calendar-access.html). _Sounds fun._

#### Ubuntu Touch Newsletters
* [All rejoice! Your Biweekly UBports news is here! :-) | UBports](https://ubports.com/blog/ubports-news-1/post/all-rejoice-your-biweekly-ubports-news-is-here-3850).

#### SailfishOS
* flypig: [Sailfish Community News, 16th June, Whisperfish](https://forum.sailfishos.org/t/sailfish-community-news-16th-june-whisperfish/11938). _In case you forgot: Whisperfish is the native Signal option for Sailfish OS._


#### Capyloon
Capyloon (a continuation of Firefox OS) has seen [another release](https://capyloon.org/releases.html#june-10). _I actually don't understand enough about web3-related terms to explain the changelog, so just click the link._

#### Flatpak
* Jakub Steiner: [Flatpak Brand Refresh](https://blog.jimmac.eu/2022/flatpak-refresh/). _Looking good!_
* Will Thompson: [How many Flathub apps reuse other package formats?](https://blogs.gnome.org/wjjt/2022/06/14/how-many-flathub-apps-reuse-other-package-formats/)


#### Distro news
* postmarketOS: [v22.06 Release: The One Where We Started Using Release Titles](https://postmarketos.org/blog/2022/06/12/v22.06-release/). _It's a great release! Do give it a try! Also, upgrading from release to release is now possible, I've even done a video about that._

#### Linux and Mesa
* Phoronix: [Imagination's PowerVR Open-Source Vulkan Driver Lands Hard Coding Infrastructure](https://www.phoronix.com/scan.php?page=news_item&px=PowerVR-Hard-Coding-Support).


### Worth noting
* If you've been wondering whether [replacing the Librem 5's socketed modem with another modem is actually possible](https://forums.puri.sm/t/simcom-sim7600g-h-alternative-modem-for-l5-tested/17534), the answer is yes. _Caveat: At least with the SIM7600-G-H-M.2 JR-Fi used its not totally seemless._
* In case you've been wondering what works on all these devices that are trending through Twitter with a "Look, Linux booting on i(Pad)OS hardware" tagline: [This should answer your question](https://github.com/SoMainline/linux-apple-resources).
* If you are using Signal Desktop as a Flatpak, [read this](https://fosstodon.org/@elagost/108488126940456875) and adjust your remotes.

### Worth reading

#### Hardware!
* Liliputing: [Anbernic RG353P is a retro gaming handheld that dual boots Android and Linux](https://liliputing.com/2022/06/anbernic-rg353p-is-a-retro-gaming-handheld-that-dual-boots-android-and-linux.html). _Check the video by ETAPRIME below. RK3566 is what makes this interesting to me._
* Hackaday: [Notkia: Building An Open And Linux-Powered Numpad Phone](https://hackaday.com/2022/06/18/notkia-building-an-open-and-linux-powered-numpad-phone/). _More on the Notkia!_

#### postmarketOS is awesome
* The Register: [postmarketOS 22.06 aims to revive end-of-life smartphones](https://www.theregister.com/2022/06/15/postmarketos_2206/). _Minor [Nitpick](https://fosstodon.org/@linmob/108482896954287447)._

#### TL;DR
* LINMOB.net: [Two Years of Life with PinePhone](https://linmob.net/two-years-of-owning-a-pinephone/). _Just a random post to celebrate and share my experiences. I am trying to follow it up with a video, see [on the post](https://linmob.net/two-years-of-owning-a-pinephone/#call-for-feedback-and-questions) and [on YouTube](https://www.youtube.com/post/UgkxYAR79bP_btyXmENFjFV-tVyTbAUGXgyV) – input appreciated!_

#### LibreOffice on PinePhone
* Peter Gama: [Demonstrations of LibreOffice on the PinePhone](https://petergamma.org/demonstrations-of-libreoffice-on-the-pinephone/). _See also the related thread on the PINE64 forums:
[Struggle to install LibreOffice on the PinePhone](https://forum.pine64.org/showthread.php?tid=16614&page=3)._

#### Matrix critique
* anarcat: [Matrix notes](https://anarc.at/blog/2022-06-17-matrix-notes/).

### Worth listening (or watching)
* Destination Linux: [282: Linux on Smartphones & GNOME Shell on Mobile - YouTube](https://www.youtube.com/watch?v=4TfxRvKVjB8), [Audio release](https://tuxdigital.com/podcasts/destination-linux/dl-282/). _Great enthusiasm!_ 

### Worth watching


#### Installing Modem Firmware
* Martijn Braam: [Updating to the Biktorgj firmware on the PinePhone - SpacePub](https://spacepub.space/w/8J45UrWr6QdRZ1MpzLRkxp). _Nice explainer!_

#### PinePhone Ethernet for Privacy
* RTP: [Pinephone Internet With Wireless Hardware Disabled (Ethernet)](https://www.youtube.com/watch?v=35cAWYJzLQI).

#### Upgrading postmarketOS
* LINMOB.net: [Upgrading to postmarketOS 22.06 (Phosh) on PinePhone](https://tilvids.com/w/ohK7QfPp5Ej8Q1Y9GucLCR), [YouTube](https://www.youtube.com/watch?v=zKZj9aQmAjA). _Do as I say, not do as I do._

#### This week in postmarketOS booting on:
* Nick Chan: [postmarketOS booting on Apple iPad Pro 9.7 Inch (Wi-Fi) (To GUI) - YouTube](https://www.youtube.com/watch?v=Kj965TW8S18). _It does not do much yet, but..._

#### SailfishOS 
* YeehawItsJake: [Sailfish OS on the Xperia 10 II - A Quick Look - YouTube](https://www.youtube.com/watch?v=a8AvyiCD64Q).

#### Gaming handhelds
* ETA Prime: [A New Dual Boot Retro Hand-Held, Is It Fast Enough? ANBERNIC RG353P First look - YouTube](https://www.youtube.com/watch?v=vCEdhWW8OB8). _The reason I find this interesting, is that a PinePhone 2 (meaning a successor to the current, regular PinePhone), is somewhat likely to be RK3566 powered._

#### Important Message
* The Linux Experiment: [No CODING SKILLS? CONTRIBUTE anyway! - YouTube](https://www.youtube.com/watch?v=FccdqCucVSI). _Amen, Brother!_

#### Release videos
* KDE: [Plasma 5.25: Amazement Guaranteed](https://tube.kockatoo.org/w/e6sZrGQZPcmk2L1T3ZxDga). _Tablets._

#### Volla Community Days
* [Volla Community Days 2022 LIVE - Day 1 - YouTube](https://www.youtube.com/watch?v=BVaykbD_K68&t=7287s). _A lot in this, I've time stamped Florians UBports talk._
* [Volla Community Days 2022 - Day 2 - YouTube](https://www.youtube.com/watch?v=TYIflikY3Ds&t=5344s). _Same here, this time the timestamp points to where Linux Muliboot is being demoed._

#### Retro
* Rene Rebe: [UNBRICKED my OLPC-XO1 battery! But so DISAPPOINTED by UPSTREAM Linux kernel suport! - YouTube](https://www.youtube.com/watch?v=BSXiP_LkikI). _I've always had a soft spot for OLPC hardware._
* Tactical PandaLE: [Dosbox on the Librem 5](https://rumble.com/v18yufr-dosbox-on-the-librem-5.html).

### Something missing?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!


### 101!
Thank you for all the kind words last week :-) _BTW: This weeks update was written on the Librem 5._
