+++
title = "Building Software from the AUR on Arch Linux ARM, Part 1: Qt /Plasma Mobile apps"
aliases = ["2020/09/05/pinephone-building-plasma-mobile-apps-from-the-aur.html"]
author = "Peter"
date = "2020-09-05T16:33:18Z"
layout = "post"
[taxonomies]
tags = ["PinePhone", "Plasma Mobile", "Arch Linux ARM", "AUR", "cmake", "Apps", "Games"]
categories = ["howto"]
authors = ["peter"]
+++
_Building software on the PinePhone?_ Sounds crazy? It sure may, but unless you have to deal with apps written in _Rust_, compilation does not take that long &mdash; the PinePhone has a quad-core CPU after all. To me, Arch Linux ARM ([project](https://archlinuxarm.org)/[PinePhone images](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)) is the obvious choice to build some programs that are listed on the [MGLapps](https://mglapps.frama.io/) list (or [similar lists](https://linmobapps.frama.io/) that just are not available otherwise. In part, that is because the Arch Linux community embraces building apps from source with the Arch User Repository (AUR), and also because of Arch's packaging approach, that does not require you to install a ton of extra `-dev` packages to build software unlike Debian, Ubuntu or Fedora do.
<!-- more -->


## Preparations

On the current build of Arch Linux ARM by danct12 upgrading can be the opposite of fun unless you reign in those locales in `/etc/locale.gen`, either by deleting or commenting out every line you don't need. 

Then, at least if you are on a 2 GB PinePhone, consider adding a swap. You can do that by adding a swap partition, using zram or both. I like adding the swap partition with Gnome Disks, this time I chose a size of 3 GB on my microSDXC card. After that, just add it to `/etc/fstab`. If you don't know how to do that, do read the articles on [Swap](https://wiki.archlinux.org/index.php/Swap) and [fstab](https://wiki.archlinux.org/index.php/Fstab) on the Arch Wiki. Also, there is `zramswap`, which can be installed via `sudo pacman -S zramswap`. After that, I adjusted my `/etc/zramswap.conf` to look like this: 

~~~~~
ZRAM_SIZE_PERCENT=50
ZRAM_COMPRESSION_ALGO=lz4
~~~~~

To install/build software from the AUR, we need an AUR-helper. I like `yay`.
Before installing `yay` let's first upgrade the system and install necessary packages: `sudo pacman -S fakeroot git make gcc go binutils`.

Please be careful and do try to read the `pkgbuild` before building software, as you might otherwise end up with malware on your system. This may seem hard at first, but eventually you'll get the hang of it and gain the benefit of adjusting or making your own pkgbuild's.

Create a folder for building software and change to that folder (I usually build stuff in `~/build`, which you would create and change to by running `mkdir -p ~/build && cd ~/build`). Now get `yay-bin`  by running `git clone https://aur.archlinux.org/yay-bin.git` and build and install it by running `cd yay` to change to the directory and then running `makepkg -si`. (This also works with every other _package_, so if you are feeling uneasy about using `yay` or need to adjust a little more to build a _package_, you can clone the pkgbuild of any AUR package by running `git clone https://aur.archlinux.org/_package_.git`).

## Installing software from the AUR

Let's start with some Plasma Mobile apps. Let's install a couple of common build dependencies first:
`sudo pacman -S qt5-svg knotifications kdbusaddons kservice kcmutils purpose cmake breeze breeze-icons plasma-framework`.

To make Qt applications use the `breeze` theme which matters if you don't want to end up with invisible buttons because of missing icons), you need to set it. One way to do so is to add the line `QT_STYLE_OVERRIDE=breeze` to `/etc/environments/`. Also, for certain Plasma Mobile apps, like [KClock](https://invent.kde.org/plasma-mobile/kclock) (a clock app) adding `QT_QUICK_CONTROLS_MOBILE=true` and `QT_QUICK_CONTROLS_STYLE=Plasma` is recommended.

`cmake` adds an item in your launcher you will likely never need. Just add a new line `NoDisplay=true` to `/usr/share/applications/cmake-gui.desktop`

### Calindori

As of writing this article, the [Calindori](https://invent.kde.org/plasma-mobile/calindori) pkgbuild lacks dependencies (I guess they assume to have all of Plasma Desktop installed), but we've taken care of them above, so the following is enough: `yay -S calindori-git `.

![Calindori: Overview](20200906_22h04m33s_grim.jpg)
![Calindori: Menu](20200906_22h00m59s_grim.jpg)
![Calindori: Add Task](20200906_22h01m10s_grim.jpg)
![Calindori: Add Task 2](20200906_22h01m36s_grim.jpg)
![Calindori: Task Overview](20200906_22h02m21s_grim.jpg)
![Calindori: Calendar menu](20200906_22h02m33s_grim.jpg)
![Calindori: Add Event](20200906_22h03m19s_grim.jpg)
![Calindori: Event Overview](20200906_22h04m43s_grim.jpg)



### Angelfish (browser)

QtWebEngine may be based on Chromium, but it makes for good browser, and [Angelfish](https://invent.kde.org/plasma-mobile/plasma-angelfish), while marked experimental, is a good enough browser. A simple `yay -S plasma-angelfish` should get it up and running.

If you want to set it your default browser, just run `xdg-settings set default-web-browser org.kde.mobile.angelfish.desktop`.[^1]

![Angelfish: Browsing](20200906_22h05m58s_grim.jpg)
![Angelfish: Tab View](20200906_22h05m52s_grim.jpg)
![Angelfish: Options](20200906_22h06m31s_grim.jpg)
![Angelfish: Options 2](20200906_22h06m42s_grim.jpg)

### KWeather 

Gnome Weather is fine after a `scale-to-fit`, but [KWeather](https://invent.kde.org/plasma-mobile/kweather) is just a bit better. It just is a `yay -S kweather-git` away.

![KWeather: Overview](20200906_22h08m29s_grim.jpg)
![KWeather: Adding another location](20200906_22h09m15s_grim.jpg)
![KWeather: Wheather information](20200906_22h09m26s_grim.jpg)
![KWeather: About screen](20200906_22h09m40s_grim.jpg)

### Index (file manager)

Imho, this is the best file manager for the PinePhone:
`yay -S index-fm-git`
Because `mauikit-git` also needs to be built from source this build takes a while.

![Index: Start screen](20200906_22h07m23s_grim.jpg)
![Index: About screen](20200906_22h07m41s_grim.jpg)

### Alligator (rss client)

If you need a simple RSS client, [Alligator](https://invent.kde.org/plasma-mobile/alligator) may be for you:
`yay -S alligator-git`

![Alligator: Start](20200906_22h13m17s_grim.jpg)
![Alligator: Feed overview](20200906_22h13m31s_grim.jpg)
![Alligator: Article view](20200906_22h13m55s_grim.jpg)
![Alligator: About](20200906_22h14m19s_grim.jpg)


## Fresh from the git

### Kongress

Let's say you want to see the schedule of [Akademy 2020](https://akademy.kde.org/2020), which of this writing is already going on. There is an app named [Kongress](https://invent.kde.org/utilities/kongress) for that, but it is not in the AUR, nor in the repos: 

You can install it manually:

~~~~~
git clone https://invent.kde.org/utilities/kongress.git
cd kongress
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr/local ..
make -j$(nproc)
sudo make install
~~~~~

![Kongress: Conference View](20200906_22h12m52s_grim.jpg)
![Kongress: Schedule Overview](20200906_22h12m30s_grim.jpg)
![Kongress: Menu](20200906_22h12m24s_grim.jpg)
![Kongress: Talk view](20200906_22h12m40s_grim.jpg)

### Plasma Samegame

This is a simple [Samegame](https://invent.kde.org/plasma-mobile/plasma-samegame), and installing works just like this: 

~~~~~~
git clone https://invent.kde.org/plasma-mobile/plasma-samegame.git
cd plasma-samegame
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr/local ..
make -j$(nproc)
sudo make install
~~~~~~

![Samegame: Start screen](20200906_22h15m44s_grim.jpg)
![Samegame: Game](20200906_22h15m48s_grim.jpg)
![Samegame: Add Highscore](20200906_22h17m08s_grim.jpg)

### Plasma Settings

In order to use Calindori with online accounts, this settings application appears to be necessary. It requires some more dependencies to build, I installed `kdelibs4support`, `kdoctools`, `kinit`, `kdesignerplugin`. After that, is was a similar procedure, only starting with a `git clone https://invent.kde.org/plasma-mobile/plasma-settings.git`.

![Settings: Overview](20200906_22h18m02s_grim.jpg)
![Settings: Add account](20200906_22h18m09s_grim.jpg)
![Settings: Add account](20200906_22h18m12s_grim.jpg)
![Settings: Date and Time](20200906_22h18m22s_grim.jpg)


Unfortunately, even with this app, I still can't add an online account to Calindori (and I could not find reliable information on whether online accounts are implemented in Calindori yet), so you should only try this if you want to have a third app called _Settings_ next to the Android settings app (which comes with Anbox) and the Gnome Settings app.

## Caveats and Conclusion

As you can hopefully see on the screenshots, the apps are using icons from at least two icon sets currently: Adwaita and Breeze. I guess that there is an environment variable that could be set to mitigate this. Also, `Index` looks a bit broken and seems to suffer from a "white text on white" issue. In a [previous post on using Qt apps on Phosh](http://linmob.net/2020/08/01/pinephone-daily-driver-challenge-part4-crossing-desktop-environment-boudaries.html) I had taken a different approach to theming with `qt5ct` which makes some of these issues probably easier to fix. Aside from that, while they don't totally fit in, these Plasma Mobile programs just work very well on Phosh. Being build with a "mobile first" approach, they are quickly compiled from source (this is not true for every GTK app, specifically for those built with Rust). Have fun trying them!

[^1]: The section on how to make Angelfish your default browser was added on October 10th, 2020.
